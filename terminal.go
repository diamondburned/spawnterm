package spawnterm

import (
	"errors"
	"os"
	"os/exec"
	"strings"
)

var (
	// ErrNoTerminalsFound is returned when can't find any terminals
	ErrNoTerminalsFound = errors.New("No terminals found, please set a $TERMINAL")
)

var (
	// DefaultTerm is the variable
	DefaultTerm string

	// CommonTerminals is a list of common and known terminals
	CommonTerminals = []string{
		"konsole",
		"gnome-terminal",
		"tilix",
		"lxterminal",
		"termite",
		"xfce4-terminal",
		"st",
		"xst",
		"alacritty",
		"kitty",
		"xterm",
		"urxvt",
		// jesus fucking christ
		"hyper",
	}
)

// FindAllTerminals attempts to find all terminals
func FindAllTerminals() (string, error) {
	term := os.Getenv("TERMINAL")
	if term != "" {
		return term, nil
	}

	c, err := exec.Command("xdg-gsettings get org.gnome.desktop.default-applications.terminal exec").Output()
	if err == nil {
		return strings.Trim(string(c), "'"), nil
	}

	x, err := exec.LookPath("x-terminal-emulator")
	if err == nil {
		return x, nil
	}

	for _, t := range CommonTerminals {
		x, err := exec.LookPath(t)
		if err != nil {
			continue
		}

		return x, nil
	}

	return "", ErrNoTerminalsFound
}

// SetTerminal attempts to find and set terminal `t`, falling
// back to the default if not found. True is returned if
// the terminal is found. A path is accepted.
func SetTerminal(t string) (bool, error) {
	// If `t` is a path.
	if strings.Contains(t, "/") {
		d, err := os.Stat(t)
		if err == nil {
			if m := d.Mode(); !m.IsDir() && m&0111 != 0 {
				DefaultTerm = t
				return true, nil
			}
		}
	}

	x, err := exec.LookPath(t)
	if err == nil {
		DefaultTerm = x
		return true, nil
	}

	x, err = FindAllTerminals()
	if err != nil {
		return false, err
	}

	DefaultTerm = x
	return false, nil
}
