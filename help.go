package spawnterm

import "os"

// IsInTerm checks if you're in a terminal
func IsInTerm() bool {
	return os.Getenv("SHELL") == ""
}
