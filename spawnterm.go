package spawnterm

import "os/exec"

// Terminal is the struct for a terminal
type Terminal struct {
	Cmd *exec.Cmd
}

// NewTerminal spawns a new terminal
func NewTerminal(command string) (*Terminal, error) {
	if DefaultTerm == "" {
		t, err := FindAllTerminals()
		if err != nil {
			return nil, err
		}

		DefaultTerm = t
	}

	c := exec.Command(DefaultTerm, "-e", command)

	return &Terminal{Cmd: c}, c.Run()
}

// Kill kills the terminal
func (t *Terminal) Kill() error {
	return t.Cmd.Process.Kill()
}
